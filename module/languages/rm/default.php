<?php

/**
 * Translations are managed using Transifex. To create a new translation
 * or to help to maintain an existing one, please register at transifex.com.
 *
 * @link    http://help.transifex.com/intro/translating.html
 * @link    https://www.transifex.com/projects/p/avisota-contao/language/rm/
 *
 * @license http://www.gnu.org/licenses/lgpl-3.0.html LGPL
 *
 * last-updated: 2014-03-11T16:04:01+01:00
 */

$GLOBALS['TL_LANG']['MCE']['headline']['0'] = 'Titel';
$GLOBALS['TL_LANG']['MCE']['headline']['1'] = 'Generescha in titel (h1 - h6).';
